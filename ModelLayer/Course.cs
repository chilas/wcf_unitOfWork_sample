﻿using System;
using System.Collections.Generic;

namespace ModelLayer
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DatePublished { get; set; }
        public IEnumerable<Author> CoAuthors { get; set; }
        public decimal FullPrice { get; set; }
        public bool Published { get; set; }

        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}
