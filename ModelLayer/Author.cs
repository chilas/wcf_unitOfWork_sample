﻿using System.Collections.Generic;

namespace ModelLayer
{
    public class Author
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Biography { get; set; }

        public IEnumerable<Course> Courses { get; set; }
    }
}
