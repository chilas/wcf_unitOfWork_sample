﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataLayer;
using ModelLayer;

namespace BusinessLayer.Services
{
    public class ServiceBase :
        AuthorService,
        CourseService
    {
        private readonly UnitOfWork _unitOfWork;

        public ServiceBase()
        {
            _unitOfWork = new UnitOfWork(new ApplicationDbContext());
        }

        public List<Author> GetAllAuthors(int pageIndex, int pageSize)
        {
            return _unitOfWork.Authors
                .GetAll(a => a.Firstname, pageIndex, pageSize)
                .ToList();
        }

        public List<Author> GetAllAuthorsWithPulishedCourses(int pageIndex, int pageSize)
        {
            return _unitOfWork.Authors
                .GetAllAuthorsWithPulishedCourses(pageIndex, pageSize)
                .ToList();
        }

        public Author CreateAuhor(Author author)
        {
            _unitOfWork.Authors
                .Add(author);
            var success = _unitOfWork.Complete();
            if (success == 1)
                return author;
            throw new InvalidDataException("The information entered does not correspond with the requirement");
        }

        public List<Course> GetAllCourses(int pageIndex, int pageSize)
        {
            return _unitOfWork.Courses
                .GetAll(c => c.Name, pageIndex, pageSize)
                .ToList();
        }

        public Course GetCourse(int id)
        {
            return _unitOfWork.Courses
                .Get(id);
        }
    }
}