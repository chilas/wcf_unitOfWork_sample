﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using ModelLayer;

namespace BusinessLayer.Services
{
    [ServiceContract]
    public interface AuthorService
    {
        [OperationContract]
        List<Author> GetAllAuthors(int pageIndex, int pageSize);

        [OperationContract]
        List<Author> GetAllAuthorsWithPulishedCourses(int pageIndex, int pageSize);

        [OperationContract]
        Author CreateAuhor(Author author);
    }
}
