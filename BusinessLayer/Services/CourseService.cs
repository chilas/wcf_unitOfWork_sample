﻿using System.Collections.Generic;
using System.ServiceModel;
using ModelLayer;

namespace BusinessLayer.Services
{
    [ServiceContract]
    public interface CourseService
    {
        [OperationContract]
        List<Course> GetAllCourses(int pageIndex, int pageSize);

        [OperationContract]
        Course GetCourse(int id);
    }
}
