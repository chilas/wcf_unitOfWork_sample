﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLayer.Interfaces;
using ModelLayer;

namespace DataLayer.Repositories
{
    class AuthorRepository: Repository<Author>, IAuthorRepository
    {
        public AuthorRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public IEnumerable<Author> GetAllAuthorsWithPulishedCourses(int pageIndex, int pageSize = 10)
        {
            return ApplicationDbContext.Authors
                .Where(a => a.Courses.Any(c => c.Published))
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }

        public ApplicationDbContext ApplicationDbContext => Context as ApplicationDbContext;
    }
}
