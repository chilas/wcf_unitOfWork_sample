﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLayer.Interfaces;
using ModelLayer;

namespace DataLayer.Repositories
{
    public class CourseRepository: Repository<Course>, ICourseRepository
    {
        public CourseRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public IEnumerable<Course> GetTopSellingCourses(int count)
        {
            return ApplicationDbContext.Courses
                .OrderByDescending(c => c.FullPrice)
                .Take(count)
                .ToList();
        }

        public IEnumerable<Course> GetCoursesWithAuthors(int pageIndex, int pageSize = 10)
        {
            return ApplicationDbContext.Courses
                .Include(c => c.Author)
                .OrderBy(c => c.Name)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }

        public ApplicationDbContext ApplicationDbContext => Context as ApplicationDbContext;
    }
}
