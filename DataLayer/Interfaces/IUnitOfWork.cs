﻿using System;

namespace DataLayer.Interfaces
{
    public interface IUnitOfWork: IDisposable
    {
        ICourseRepository Courses { get; }
        IAuthorRepository Authors { get; }
        int Complete();
    }
}