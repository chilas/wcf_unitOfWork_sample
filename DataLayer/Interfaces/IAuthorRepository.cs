﻿using System.Collections.Generic;
using ModelLayer;

namespace DataLayer.Interfaces
{
    public interface IAuthorRepository: IRepository<Author>
    {
        IEnumerable<Author> GetAllAuthorsWithPulishedCourses(int pageIndex, int pageSize = 10);
    }
}
