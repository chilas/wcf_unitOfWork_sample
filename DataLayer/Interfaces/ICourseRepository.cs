﻿using System.Collections.Generic;
using ModelLayer;

namespace DataLayer.Interfaces
{
    public interface ICourseRepository: IRepository<Course>
    {
        IEnumerable<Course> GetTopSellingCourses(int count);
        IEnumerable<Course> GetCoursesWithAuthors(int pageIndex, int pageSize);
    }
}
